# Copyright 2018 Mats Sjöberg

# This file is part of the getpods programme.
#
# getpods is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# getpods is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with getpods.  If not, see <http://www.gnu.org/licenses/>.

import collections
import re

import feedparser


class Item(object):
    """Class encapsulating the information for a single podcast item,
    i.e. a single episode.  Also contains a static cache keeping track
    of already downloaded episodes.
    """

    cache = collections.OrderedDict()
    cache_read = False
    cache_file = "cache"

    def __init__(self, item_data, feed):
        if not Item.cache_read:
            Item.read_cache()

        self.data = item_data
        self.feed = feed

        summ = self.data["summary"]
        summ = re.sub('<[^<]+?>', ' ', summ)
        summ = summ.replace('&#38;', '&').replace('&#8230;', '...')
        summ = re.sub('&#?[0-9a-z]+;', '', summ)
        summ = re.sub('[ \t]+', ' ', summ)
        summ = re.sub('\n+', '\n', summ)
        self.summary = summ

    def __str__(self):
        return "[{0}] {1}".format(self.feed.title(),
                                  self.title())

    def is_new(self):
        return self.guid() not in Item.cache

    def mark_as_seen(self):
        Item.cache[self.guid()] = self.title()

    def guid(self):
        """Method that determines and return a unique identifier of
        the item."""

        return self.data["guid"]

    def author(self):
        if "author" in self.data:
            return self.data["author"]
        return ""

    def title(self):
        return self.data["title"]

    def auto_download(self):
        return self.feed.do_auto

    def download_url(self):
        url = ""
        if "media_content" in self.data:
            for media in self.data["media_content"]:
                if "url" in media:
                    url = media["url"]
        elif "enclosure" in self.data:
            url = self.data["enclosure"]["url"]
        elif "links" in self.data:
            for link in self.data["links"]:
                if link["rel"] == "enclosure":
                    url = link["href"]

        return url

    def download_localname(self):
        dl_url = self.download_url()
        parts = dl_url.rpartition('.')
        ext = parts[2].partition('?')[0]
        return re.sub('[^a-zA-Z0-9]', '_', self.title()) + '.' + ext

    def print_summary(self, max_summary_lines):
        # clear_screen()
        print("\n\n*", self)
        sumlines = self.summary.splitlines()
        summary = "\n".join(sumlines[0:max_summary_lines])
        if len(sumlines) > max_summary_lines:
            summary += "\n..."
        author = self.author()
        if author != "":
            summary = "Author: " + author + "\n" + summary
        print(summary)

    @staticmethod
    def setup_cache(dir):
        Item.cache_file = dir+"/cache"

    @staticmethod
    def save_cache():
        """Saves cache from memory to file."""

        with open(Item.cache_file, "w") as fp:
            for guid, title in Item.cache.items():
                fp.write(guid + '\t' + title + '\n')

    @staticmethod
    def read_cache():
        """Reads old cache from file to memory."""

        try:
            with open(Item.cache_file) as fp:
                for line in fp:
                    (guid, title) = line.rstrip().split('\t')
                    Item.cache[guid] = title
        except:
            pass
        Item.cache_read = True


class Feed(object):
    """
    Class encapsulating the information for a podcast feed.
    """

    def __init__(self, url, dirname, do_auto):
        self.url = url
        self.dirname = dirname
        self.do_auto = do_auto
        self.data = {}

    def update(self, newest=0):
        self.data = feedparser.parse(self.url)

        title = self.title()
        if not title:
            print("Error updating", self.url)
            return []

        print("Updating", self.title(), "...")

        new_items = []
        count = 0
        for item_data in self.data["items"]:
            item = Item(item_data, self)
            if item.is_new():
                if newest == 0 or count < newest:
                    new_items.append(item)
                    count += 1
                else:
                    item.mark_as_seen()

        # Item.save_cache()
        return new_items

    def title(self):
        if "title" in self.data["channel"]:
            return self.data["channel"]["title"]
        else:
            return ""
