getpods.py is a very simple command line podcast client in Python, that I made for my personal use.

# Requirements

As of June 25, 2018 it only supports Python 3. I'm too old to keep messing with unicode issues, etc.

The `feedparser` module needs to be installed, for example as `apt install python3-feedparser` in Debian or Ubuntu.

# Usage and configuration

You need to create a configuration file called `~/.getpods` with this minimum content (change the podcast directory as appropriate):

    [general]
    podcasts_dir=~/Podcasts

There is a sample configuration file showing all the options called `getpods.sample.conf` distributed together with this script that you can copy and modify.

In the `podcast_dir` directory, create a file called `urls` which contains one line for each podcast feed with the following format:

    # comments preceeded by hash
    url directory_name [?]

For example:

    # These are my awesome podcast feeds
    http://faif.us/feeds/cast-ogg/ faif
    http://hackerpublicradio.org/hpr_ogg_rss.php hpr ?

The question mark is for feeds where the program should query about each new episode.  Other feeds have new episodes automatically downloaded.

Episodes that have been downloaded or otherwise marked as "seen" are registered in a file called "cache" in the podcast_dir. 
